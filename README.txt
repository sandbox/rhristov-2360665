******************************************************************************

Description:
-------------------------------------------------------------------------------
The Drupal Logger module provides tools for experienced site monitoring 
and custom logging. 

For each log you became information about:
 - Date and time of log capturing
 - Log message
 - Type of the message
 - Severity of the message
 - User 
 - Referer
 - Ip address
 - URL address
 - Referer
 - $_GET parameters
 - $_POST parameters
 - $_SESSION parameters
 - $_COOKIE parameters
 - Stacktrace without arguments

Realtime log page (admin/reports/drupalog) is available where without browser 
refresh latest logs are displayed.
Possibilities for clearing current logs data and 
filtering by type and severity.

Archive page (admin/reports/drupalog/archive) is available where you can 
search in all logs by message, severity, type, user, ip address, $_GET, 
$_POST, $_SESSION and $_COOKIE variables.

Settings page (admin/reports/drupalog/settings) is available where you can 
configure realtime logs page and set email notification configuration and 
so you will receive log notifications by mail.

There is hook on watchdog and all logs recorded with call to function watchdog 
are visible and in Drupal logger.

Installation:
-------------------------------------------------------------------------------
1.  Enable module in module list located at administer > build > modules.

Custom logging:
-------------------------------------------------------------------------------
\Drupalog::log('Some debug message.', 'my_modyle', \Drupalog::SEVERITY_DEBUG);
\Drupalog::log('Some notice message.', 'my_modyle', \Drupalog::SEVERITY_NOTICE);
\Drupalog::log(
  'Some warning message.', 'my_modyle', \Drupalog::SEVERITY_WARNING
);
\Drupalog::log('Some error message.', 'my_modyle', \Drupalog::SEVERITY_ERROR);

For capturing managed expection:
try {
//Your code here.
} catch (\Exception $exception) {
  \Drupalog::exception($exception, $managed_exception = TRUE);
}
