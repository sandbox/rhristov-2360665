<?php

/**
 * Implements hook_schema().
 */
function drupalog_schema() {
  return array(
    'drupalog' => array(
      'description' => 'Base table for storing drupal logs.',
      'fields' => array(
        'did' => array(
          'description' => 'The primary table identifier.',
          'type' => 'serial',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
        'message' => array(
          'description' => 'The logged message.',
          'type' => 'text',
          'size' => 'medium',
          'not null' => TRUE,
        ),
        'type' => array(
          'description' => 'The log record type.',
          'type' => 'varchar',
          'length' => 50,
          'not null' => TRUE,
        ),
        'severity' => array(
          'description' => 'The log record severity type.',
          'type' => 'int',
          'size' => 'small',
          'not null' => TRUE,
        ),
        'date_time' => array(
          'description' => 'The Unix timestamp when log is created.',
          'type' => 'int',
          'not null' => TRUE,
        ),
        'ip_address' => array(
          'description' => 'The log record ip address.',
          'type' => 'varchar',
          'length' => 15,
          'not null' => TRUE,
        ),
        'uid' => array(
          'description' => 'The log record user ID.',
          'type' => 'int',
          'not null' => FALSE,
        ),
        'url' => array(
          'description' => 'The URL.',
          'type' => 'varchar',
          'length' => 1000,
          'not null' => TRUE,
        ),
        'http_referer' => array(
          'description' => 'The HTTP_REFERER.',
          'type' => 'varchar',
          'length' => 1000,
          'not null' => FALSE,
        ),
        'stacktrace' => array(
          'description' => 'The serialized stacktrace.',
          'type' => 'blob',
          'size' => 'normal',
          'serialize' => TRUE,
          'not null' => FALSE,
        ),
        'get_request' => array(
          'description' => 'The GET serialized variables.',
          'type' => 'blob',
          'size' => 'normal',
          'serialize' => TRUE,
          'not null' => TRUE,
        ),
        'post_request' => array(
          'description' => 'The POST serialized variables.',
          'type' => 'blob',
          'size' => 'normal',
          'serialize' => TRUE,
          'not null' => TRUE,
        ),
        'session' => array(
          'description' => 'The SESSION serialized variables.',
          'type' => 'blob',
          'size' => 'normal',
          'serialize' => TRUE,
          'not null' => TRUE,
        ),
        'cookie' => array(
          'description' => 'The COOKIE serialized variables.',
          'type' => 'blob',
          'size' => 'normal',
          'serialize' => TRUE,
          'not null' => TRUE,
        ),
      ),
      'primary key' => array('did'),
    ),
  );
}

/**
 * Implements hook_install().
 */
function drupalog_install() {
  variable_set('drupalog', array(
    'logs_preview_count' => 100,
    'logs_minimal_severity' => 1,
    'mails_addresses' => NULL,
    'mails_minimal_severity' => 6,
    'mails_last_log_id' => 0,
  ));
}

/**
 * Implements hook_uninstall().
 */
function drupalog_uninstall() {
  variable_del('drupalog');
}
