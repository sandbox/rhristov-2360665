<?php

class DrupalogEntity {

  protected static $table_name = 'drupalog';
  protected $did;
  protected $message;
  protected $type;
  protected $severity;
  protected $date_time;
  protected $ip_address;
  protected $uid;
  protected $url;
  protected $http_referer;
  protected $stacktrace;
  protected $get_request;
  protected $post_request;
  protected $session;
  protected $cookie;

  public function __construct() {
    if (empty($this->did)) {
      $this->fillProperties();
    }
    else {
      foreach ($this as $property => $value) {
        if (in_array($property, array(
              'stacktrace',
              'get_request',
              'post_request',
              'session',
              'cookie',
            )) && is_string($value)) {
          $this->$property = unserialize($value);
        }
      }
    }
  }

  public static function getTableName() {
    return self::$table_name;
  }

  public function setMessage($message) {
    if (is_string($message)) {
      $this->message = (string) $message;
    }

    return $this;
  }

  public function setType($type) {
    if (is_string($type)) {
      $this->type = (string) $type;
    }

    return $this;
  }

  public function setSeverity($severity) {
    $this->severity = $severity;

    return $this;
  }

  public function setStacktrace($stacktrace) {
    $this->stacktrace = $stacktrace;

    return $this;
  }

  public function getDid() {
    return $this->did;
  }

  public function getMessage() {
    return $this->message;
  }

  public function getType() {
    return $this->type;
  }

  public function getSeverity() {
    return $this->severity;
  }

  public function getDateTime() {
    return $this->date_time;
  }

  public function getIpAddress() {
    return $this->ip_address;
  }

  public function getUrl() {
    return $this->url;
  }

  public function getUid() {
    return $this->uid;
  }

  public function getHttpReferer() {
    return $this->http_referer;
  }

  public function getStacktrace() {
    return $this->stacktrace;
  }

  public function getGetRequest() {
    return $this->get_request;
  }

  public function getPostRequest() {
    return $this->post_request;
  }

  public function getSession() {
    return $this->session;
  }

  public function getCookie() {
    return $this->cookie;
  }

  public function save() {
    try {
      $data = array();
      foreach ($this as $property => $value) {
        $data[$property] = $value;
      }
      drupal_write_record(self::$table_name, $data);
    }
    catch (\Exception $exception) {

    }
  }

  private function fillProperties() {
    $this->date_time = REQUEST_TIME;
    $this->ip_address = ip_address();
    $this->url = request_uri();
    $this->http_referer = filter_input(INPUT_SERVER, 'HTTP_REFERER');
    $this->uid = $GLOBALS['user']->uid ? : NULL;
    $this->get_request = (array) drupal_get_query_parameters();
    $this->post_request = (array) $_POST;
    $this->session = isset($_SESSION) ? (array) $_SESSION : array();
    $this->cookie = (array) $_COOKIE;
  }

  public function toArray($prity_print = FALSE) {
    $data = array();
    foreach ($this as $property => $value) {
      if ($prity_print) {
        if (is_array($value) && 'stacktrace' !== $property) {
          array_walk_recursive($value, function (&$value) {
            $value = wordwrap((string) $value, 75, '<br />', TRUE);
          });
        }
        elseif ('uid' === $property) {
          if ($value) {
            $user = user_load($value);
            $value = $user ? $user->name : '';
          }
          else {
            $value = '';
          }
        }
        elseif ('date_time' === $property) {
          $value = date('D, M j, Y - H:i', $value);
        }
        elseif ('severity' === $property) {
          $value = Drupalog::$severities[$value];
        }
      }
      $data[$property] = $value;
    }

    return $data;
  }

}
