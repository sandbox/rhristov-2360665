<?php

/**
 * Function for showing newest log messages.
 */
function drupalog_service_load_more($last_log_id) {
  $array_logs = array();
  $log_types = array();

  $logs = db_select(\DrupalogEntity::getTableName())
      ->fields('drupalog')
      ->condition('drupalog.did', (int) $last_log_id, '>')
      ->condition('drupalog.severity', drupalog_variable_get('logs_minimal_severity', \Drupalog::SEVERITY_DEBUG), '>=')
      ->orderBy('drupalog.did', 'ASC')
      ->range(0, drupalog_variable_get('logs_preview_count', 100))
      ->execute()
      ->fetchAll(\PDO::FETCH_CLASS, 'DrupalogEntity');
  $last_log_key = count($logs) - 1;
  foreach ($logs as $key => $log) {
    $array_logs[] = $log->toArray(TRUE);

    $log_types[$log->getType()] = $log->getType();

    if ($last_log_key === $key) {
      $last_log_id = $log->getDid();
    }
  }

  $log_types = array_values($log_types);

  drupal_json_output(array(
    'logs' => $array_logs,
    'last_log_id' => $last_log_id,
    'log_types' => $log_types,
  ));

  drupal_exit();
}
