<?php

use \Exception;
use \Drupalog;

/**
 * Final singleton class for handling writing to drupal logger table.
 */
final class Drupalog {

  /**
   * Drupalog singleton instance.
   *
   * @var Drupalog 
   */
  private static $instance;

  /**
   * Defining severity types.
   */
  const SEVERITY_DEBUG = 1;
  const SEVERITY_INFO = 2;
  const SEVERITY_NOTICE = 3;
  const SEVERITY_WARNING = 4;
  const SEVERITY_ERROR = 5;
  const SEVERITY_CRITICAL = 6;

  /**
   * List of all severity types.
   *
   * @var array 
   */
  public static $severities = array(
    self::SEVERITY_DEBUG => 'Debug',
    self::SEVERITY_INFO => 'Info',
    self::SEVERITY_NOTICE => 'Notice',
    self::SEVERITY_WARNING => 'Warning',
    self::SEVERITY_ERROR => 'Error',
    self::SEVERITY_CRITICAL => 'Critical',
  );

  /**
   * Error mapping between php errors and drupalog severity statuses.
   *
   * @var array 
   */
  private static $error_mapping = array(
    E_COMPILE_ERROR => self::SEVERITY_ERROR,
    E_COMPILE_WARNING => self::SEVERITY_WARNING,
    E_CORE_ERROR => self::SEVERITY_ERROR,
    E_CORE_WARNING => self::SEVERITY_WARNING,
    E_DEPRECATED => self::SEVERITY_NOTICE,
    E_ERROR => self::SEVERITY_ERROR,
    E_NOTICE => self::SEVERITY_NOTICE,
    E_PARSE => self::SEVERITY_ERROR,
    E_RECOVERABLE_ERROR => self::SEVERITY_ERROR,
    E_STRICT => self::SEVERITY_NOTICE,
    E_USER_DEPRECATED => self::SEVERITY_WARNING,
    E_USER_ERROR => self::SEVERITY_ERROR,
    E_USER_NOTICE => self::SEVERITY_NOTICE,
    E_USER_WARNING => self::SEVERITY_WARNING,
    E_WARNING => self::SEVERITY_WARNING,
  );

  /**
   * Method for calling Drupalog.
   *
   * @return Drupalog
   * @throws Exception
   */
  public static function instance() {
    if (NULL === self::$instance) {
      throw new Exception(__CLASS__ . ' is not instantiated.');
    }

    return self::$instance;
  }

  /**
   * Method for initializing Drupalog and attaching all needed handlers.
   */
  public static function init() {
    if (NULL === self::$instance) {
      self::$instance = new static;

      set_error_handler(array('Drupalog', 'error'));
      set_exception_handler(array('Drupalog', 'exception'));
    }
  }

  /**
   * Custom exception handler method.
   *
   * @param Exception $exception
   * @param bool $handled_exception If called in catch block rise $handled_exception flag to TRUE.
   */
  public static function exception(Exception $exception, $handled_exception = FALSE) {
    $stacktrace = $exception->getTrace();
    foreach ($stacktrace as &$stack) {
      if (isset($stack['args'])) {
        unset($stack['args']);
      }
    }

    self::saveLog('Exception: <b>' . $exception->getMessage() . '</b> in <b>' . $exception->getFile() . '</b> line <b>' . $exception->getLine() . '</b>.', 'php', self::SEVERITY_CRITICAL, $stacktrace);

    if (FALSE === $handled_exception) {
      try {
        require_once DRUPAL_ROOT . '/includes/errors.inc';

        _drupal_log_error(_drupal_decode_exception($exception), TRUE);
      }
      catch (Exception $inner_exception) {
        
      }
    }
  }

  /**
   * Custom error handler method.
   *
   * @param string $errno
   * @param string $errstr
   * @param string $errfile
   * @param string $errline
   */
  public static function error($errno, $errstr, $errfile, $errline) {
    //In devel module there is not well suppressed error triggered on each request.
    if (strpos($errstr, 'fb.php') !== FALSE && strpos($errstr, 'include_once') !== FALSE) {
      return;
    }
    $stacktrace = self::formatStacktrace(version_compare(PHP_VERSION, '5.3.6') >= 0 ? debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS) : debug_backtrace());

    self::saveLog(self::$severities[self::$error_mapping[$errno]] . ': ' . '<b>' . $errstr . '</b> in <b>' . $errfile . '</b> line <b>' . $errline . '</b>.', 'php', isset(self::$error_mapping[$errno]) ? self::$error_mapping[$errno] : self::DRUPALOG_SEVERITY_WARNING, $stacktrace);
  }

  /**
   * Custom logging.
   *
   * @param string $message The log message.
   * @param string $type The log type.
   * @param int $severity The log severity.
   */
  public static function log($message, $type, $severity = self::SEVERITY_INFO) {
    $stacktrace = self::formatStacktrace(version_compare(PHP_VERSION, '5.3.6') >= 0 ? debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS) : debug_backtrace());

    self::saveLog($message, $type, in_array($severity, array_keys(self::$severities)) ? $severity : self::DRUPALOG_SEVERITY_INFO, $stacktrace);
  }

  /**
   * Helper function for saving log entity to the database.
   *
   * @param string $message
   * @param string $type
   * @param int $severity
   */
  private static function saveLog($message, $type, $severity, array $stacktrace = array()) {
    if ($severity >= drupalog_variable_get('logs_minimal_severity', self::SEVERITY_DEBUG)) {
      $drupalog_entity = new DrupalogEntity();
      $drupalog_entity
          ->setMessage($message)
          ->setType($type)
          ->setSeverity($severity)
          ->setStacktrace($stacktrace)
          ->save();
    }
  }

  /**
   * Helper function for removing stacktrace arguments and first stack.
   *
   * @param array $stacktrace
   *
   * @return array
   */
  private static function formatStacktrace(array $stacktrace = array()) {
    array_shift($stacktrace);

    foreach ($stacktrace as &$stack) {
      if (isset($stack['args'])) {
        unset($stack['args']);
      }
    }

    return $stacktrace;
  }

  /**
   * Prevent creation of new instance.
   */
  protected function __construct() {
    
  }

  /**
   * Prevent class cloning.
   *
   * @return void
   */
  private function __clone() {
    
  }

  /**
   * Prevent class unserialization.
   *
   * @return void
   */
  private function __wakeup() {
    
  }

}
