<?php

class drupalog_views_handler_drupalog_severity_field extends views_handler_field {
  
  function pre_render(&$values) {
    foreach ($values as &$value) {
      if (!empty($value->drupalog_severity)) {
        $value->drupalog_severity = \Drupalog::$severities[$value->drupalog_severity];
      }
    }
  }
}
