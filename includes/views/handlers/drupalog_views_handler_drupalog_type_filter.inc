<?php

class drupalog_views_handler_drupalog_type_filter extends views_handler_filter_in_operator {

  function get_value_options() {
    if (empty($this->value_options)) {
      $value_options = array();
 
      $query = db_select(\DrupalogEntity::getTableName());
      $query->addField(\DrupalogEntity::getTableName(), 'type');
      $query->groupBy(\DrupalogEntity::getTableName() . '.type');
      $results = $query->execute()->fetchAll(PDO::FETCH_COLUMN);
      foreach ($results as $result) {
        $value_options[$result] = $result;
      }

      $this->value_options = $value_options;
    }
  }

}
