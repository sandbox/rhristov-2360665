<?php

class drupalog_views_handler_drupalog_severity_filter extends views_handler_filter_in_operator {

  function get_value_options() {
    if (empty($this->value_options)) {
      $this->value_options = \Drupalog::$severities;
    }
  }

}
