<?php

/**
 * Helper function for getting drupal logger views data structure.
 *
 * @return array
 */
function drupalog_views_data_get_data() {
  $data['drupalog']['table']['group'] = t('Drupal Logger');

  $data['drupalog']['table']['base'] = array(
    'field' => 'did',
    'title' => t('Drupal Logger'),
    'help' => t('Drupal Logger entry'),
    'weight' => -10,
  );

  $data['drupalog']['type'] = array(
    'title' => t('Type'),
    'help' => t('The log record type'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'drupalog_views_handler_drupalog_type_filter',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['drupalog']['did'] = array(
    'title' => t('Did'),
    'help' => t('Drupal logger identifier.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['drupalog']['severity'] = array(
    'title' => t('Severity'),
    'help' => t('The log record severity type'),
    'field' => array(
      'handler' => 'drupalog_views_handler_drupalog_severity_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'drupalog_views_handler_drupalog_severity_filter',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['drupalog']['date_time'] = array(
    'title' => t('Date'),
    'help' => t('The Unix timestamp when log is created.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  $data['drupalog']['table']['join'] = array(
    'users' => array(
      'left_field' => 'uid',
      'field' => 'uid',
    ),
  );
  $data['drupalog']['uid'] = array(
    'title' => t('Users'),
    'help' => t('Reference to users table.'),
    'relationship' => array(
      'base' => 'users',
      'base field' => 'uid',
      'handler' => 'views_handler_relationship',
      'label' => t('Users relationship'),
      'title' => t('Relationship to users table'),
    ),
  );

  foreach (array(
'message' => array('title' => 'Message', 'help' => 'The logged message', 'click_sortable' => TRUE),
 'ip_address' => array('title' => 'Ip address', 'help' => 'The log record ip address.'),
 'url' => array('title' => 'URL', 'help' => 'The URL.'),
 'http_referer' => array('title' => '$_SERVER["HTTP_REFERER"]', 'help' => 'The $_SERVER["HTTP_REFERER"].'),
 'stacktrace' => array('title' => 'Stacktrace', 'help' => 'The serialized stacktrace.'),
 'get_request' => array('title' => '$_GET', 'help' => 'The $_GET serialized variables.'),
 'post_request' => array('title' => '$_POST', 'help' => 'The $_POST serialized variables.'),
 'session' => array('title' => '$_SESSION', 'help' => 'The $_SESSION serialized variables.'),
 'cookie' => array('title' => '$_COOKIE', 'help' => 'The $_COOKIE serialized variables.'),
  ) as $name => $values) {
    $data['drupalog'][$name] = array(
      'title' => t($values['title']),
      'help' => t($values['help']),
      'field' => array(
        'handler' => !empty($values['field_handler']) ? $values['field_handler'] : 'views_handler_field',
        'click sortable' => isset($values['click_sortable']) ? (bool) $values['click_sortable'] : FALSE,
      ),
      'filter' => array(
        'handler' => !empty($values['filter_handler']) ? $values['filter_handler'] : 'views_handler_filter_string',
      ),
      'argument' => array(
        'handler' => !empty($values['argument_handler']) ? $values['argument_handler'] : 'views_handler_argument_string',
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
    );
  }

  return $data;
}
