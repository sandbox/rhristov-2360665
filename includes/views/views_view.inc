<?php

/**
 * Helper function for getting drupal logger view.
 *
 * @return array
 */
function drupalog_views_view_get_view() {
  $view = new view();
  $view->name = 'drupal_logger_archive';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'drupalog';
  $view->human_name = 'Drupal logger archive';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Archive';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'access drupalog logs';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'better_exposed_filters';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Filter';
  $handler->display->display_options['exposed_form']['options']['reset_button'] = TRUE;
  $handler->display->display_options['exposed_form']['options']['bef'] = array(
    'general' => array(
      'allow_secondary' => 0,
      'secondary_label' => 'Advanced options',
    ),
    'message' => array(
      'bef_format' => 'default',
      'more_options' => array(
        'is_secondary' => 0,
        'any_label' => '',
        'bef_filter_description' => '',
        'tokens' => array(
          'available' => array(
            0 => 'global_types',
          ),
        ),
        'rewrite' => array(
          'filter_rewrite_values' => '',
        ),
      ),
    ),
    'severity' => array(
      'bef_format' => 'default',
      'more_options' => array(
        'bef_select_all_none' => 0,
        'bef_collapsible' => 0,
        'is_secondary' => 0,
        'any_label' => '',
        'bef_filter_description' => '',
        'tokens' => array(
          'available' => array(
            0 => 'global_types',
          ),
        ),
        'rewrite' => array(
          'filter_rewrite_values' => '',
        ),
      ),
    ),
    'type_2' => array(
      'bef_format' => 'default',
      'more_options' => array(
        'bef_select_all_none' => 0,
        'bef_collapsible' => 0,
        'is_secondary' => 0,
        'any_label' => '',
        'bef_filter_description' => '',
        'tokens' => array(
          'available' => array(
            0 => 'global_types',
          ),
        ),
        'rewrite' => array(
          'filter_rewrite_values' => '',
        ),
      ),
    ),
    'uid' => array(
      'bef_format' => 'default',
      'more_options' => array(
        'bef_select_all_none' => FALSE,
        'bef_collapsible' => 0,
        'is_secondary' => 0,
        'any_label' => '',
        'bef_filter_description' => '',
        'tokens' => array(
          'available' => array(
            0 => 'global_types',
          ),
        ),
        'rewrite' => array(
          'filter_rewrite_values' => '',
        ),
      ),
    ),
    'ip_address' => array(
      'bef_format' => 'default',
      'more_options' => array(
        'is_secondary' => 0,
        'any_label' => '',
        'bef_filter_description' => '',
        'tokens' => array(
          'available' => array(
            0 => 'global_types',
          ),
        ),
        'rewrite' => array(
          'filter_rewrite_values' => '',
        ),
      ),
    ),
    'date_time' => array(
      'bef_format' => 'bef_datepicker',
      'more_options' => array(
        'is_secondary' => 0,
        'any_label' => '',
        'bef_filter_description' => '',
        'tokens' => array(
          'available' => array(
            0 => 'global_types',
          ),
        ),
        'rewrite' => array(
          'filter_rewrite_values' => '',
        ),
      ),
    ),
    'date_time_1' => array(
      'bef_format' => 'bef_datepicker',
      'more_options' => array(
        'is_secondary' => 0,
        'any_label' => '',
        'bef_filter_description' => '',
        'tokens' => array(
          'available' => array(
            0 => 'global_types',
          ),
        ),
        'rewrite' => array(
          'filter_rewrite_values' => '',
        ),
      ),
    ),
    'get_request' => array(
      'bef_format' => 'default',
      'more_options' => array(
        'is_secondary' => 0,
        'any_label' => '',
        'bef_filter_description' => '',
        'tokens' => array(
          'available' => array(
            0 => 'global_types',
          ),
        ),
        'rewrite' => array(
          'filter_rewrite_values' => '',
        ),
      ),
    ),
    'post_request' => array(
      'bef_format' => 'default',
      'more_options' => array(
        'is_secondary' => 0,
        'any_label' => '',
        'bef_filter_description' => '',
        'tokens' => array(
          'available' => array(
            0 => 'global_types',
          ),
        ),
        'rewrite' => array(
          'filter_rewrite_values' => '',
        ),
      ),
    ),
    'cookie' => array(
      'bef_format' => 'default',
      'more_options' => array(
        'is_secondary' => 0,
        'any_label' => '',
        'bef_filter_description' => '',
        'tokens' => array(
          'available' => array(
            0 => 'global_types',
          ),
        ),
        'rewrite' => array(
          'filter_rewrite_values' => '',
        ),
      ),
    ),
    'session' => array(
      'bef_format' => 'default',
      'more_options' => array(
        'is_secondary' => 0,
        'any_label' => '',
        'bef_filter_description' => '',
        'tokens' => array(
          'available' => array(
            0 => 'global_types',
          ),
        ),
        'rewrite' => array(
          'filter_rewrite_values' => '',
        ),
      ),
    ),
  );
  $handler->display->display_options['exposed_form']['options']['input_required'] = 0;
  $handler->display->display_options['exposed_form']['options']['text_input_required_format'] = 'mail';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '100';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'did' => 'did',
    'date_time' => 'date_time',
    'message' => 'message',
    'type' => 'type',
    'severity' => 'severity',
    'name' => 'name',
    'url' => 'url',
    'http_referer' => 'http_referer',
    'ip_address' => 'ip_address',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'did' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'date_time' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'message' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'type' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'severity' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'name' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'url' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'http_referer' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'ip_address' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Relationship: Drupal Logger: Relationship to users table */
  $handler->display->display_options['relationships']['uid']['id'] = 'uid';
  $handler->display->display_options['relationships']['uid']['table'] = 'drupalog';
  $handler->display->display_options['relationships']['uid']['field'] = 'uid';
  /* Field: Drupal Logger: Did */
  $handler->display->display_options['fields']['did']['id'] = 'did';
  $handler->display->display_options['fields']['did']['table'] = 'drupalog';
  $handler->display->display_options['fields']['did']['field'] = 'did';
  $handler->display->display_options['fields']['did']['label'] = '';
  $handler->display->display_options['fields']['did']['exclude'] = TRUE;
  $handler->display->display_options['fields']['did']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['did']['separator'] = '';
  /* Field: Drupal Logger: Date */
  $handler->display->display_options['fields']['date_time']['id'] = 'date_time';
  $handler->display->display_options['fields']['date_time']['table'] = 'drupalog';
  $handler->display->display_options['fields']['date_time']['field'] = 'date_time';
  $handler->display->display_options['fields']['date_time']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['date_time']['date_format'] = 'custom';
  $handler->display->display_options['fields']['date_time']['custom_date_format'] = 'D, M j, Y - H:i';
  /* Field: Drupal Logger: Message */
  $handler->display->display_options['fields']['message']['id'] = 'message';
  $handler->display->display_options['fields']['message']['table'] = 'drupalog';
  $handler->display->display_options['fields']['message']['field'] = 'message';
  $handler->display->display_options['fields']['message']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['message']['alter']['path'] = 'admin/reports/drupalog/details/[did]';
  $handler->display->display_options['fields']['message']['alter']['trim_whitespace'] = TRUE;
  $handler->display->display_options['fields']['message']['alter']['max_length'] = '120';
  $handler->display->display_options['fields']['message']['alter']['strip_tags'] = TRUE;
  $handler->display->display_options['fields']['message']['alter']['trim'] = TRUE;
  $handler->display->display_options['fields']['message']['element_label_colon'] = FALSE;
  /* Field: Drupal Logger: Type */
  $handler->display->display_options['fields']['type']['id'] = 'type';
  $handler->display->display_options['fields']['type']['table'] = 'drupalog';
  $handler->display->display_options['fields']['type']['field'] = 'type';
  $handler->display->display_options['fields']['type']['element_label_colon'] = FALSE;
  /* Field: Drupal Logger: Severity */
  $handler->display->display_options['fields']['severity']['id'] = 'severity';
  $handler->display->display_options['fields']['severity']['table'] = 'drupalog';
  $handler->display->display_options['fields']['severity']['field'] = 'severity';
  $handler->display->display_options['fields']['severity']['element_label_colon'] = FALSE;
  /* Field: User: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'users';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['relationship'] = 'uid';
  $handler->display->display_options['fields']['name']['label'] = 'User';
  $handler->display->display_options['fields']['name']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['name']['empty'] = 'Anonym';
  /* Field: Drupal Logger: URL */
  $handler->display->display_options['fields']['url']['id'] = 'url';
  $handler->display->display_options['fields']['url']['table'] = 'drupalog';
  $handler->display->display_options['fields']['url']['field'] = 'url';
  $handler->display->display_options['fields']['url']['alter']['path'] = '[url]';
  $handler->display->display_options['fields']['url']['alter']['trim_whitespace'] = TRUE;
  $handler->display->display_options['fields']['url']['alter']['target'] = '_blank';
  $handler->display->display_options['fields']['url']['alter']['max_length'] = '40';
  $handler->display->display_options['fields']['url']['alter']['strip_tags'] = TRUE;
  $handler->display->display_options['fields']['url']['alter']['trim'] = TRUE;
  $handler->display->display_options['fields']['url']['element_label_colon'] = FALSE;
  /* Field: Drupal Logger: $_SERVER["HTTP_REFERER"] */
  $handler->display->display_options['fields']['http_referer']['id'] = 'http_referer';
  $handler->display->display_options['fields']['http_referer']['table'] = 'drupalog';
  $handler->display->display_options['fields']['http_referer']['field'] = 'http_referer';
  $handler->display->display_options['fields']['http_referer']['label'] = 'Referer';
  $handler->display->display_options['fields']['http_referer']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['http_referer']['alter']['path'] = '[http_referer]';
  $handler->display->display_options['fields']['http_referer']['alter']['external'] = TRUE;
  $handler->display->display_options['fields']['http_referer']['alter']['trim_whitespace'] = TRUE;
  $handler->display->display_options['fields']['http_referer']['alter']['target'] = '_blank';
  $handler->display->display_options['fields']['http_referer']['alter']['max_length'] = '40';
  $handler->display->display_options['fields']['http_referer']['alter']['trim'] = TRUE;
  $handler->display->display_options['fields']['http_referer']['element_label_colon'] = FALSE;
  /* Field: Drupal Logger: Ip address */
  $handler->display->display_options['fields']['ip_address']['id'] = 'ip_address';
  $handler->display->display_options['fields']['ip_address']['table'] = 'drupalog';
  $handler->display->display_options['fields']['ip_address']['field'] = 'ip_address';
  $handler->display->display_options['fields']['ip_address']['element_label_colon'] = FALSE;
  /* Sort criterion: Drupal Logger: Did */
  $handler->display->display_options['sorts']['did']['id'] = 'did';
  $handler->display->display_options['sorts']['did']['table'] = 'drupalog';
  $handler->display->display_options['sorts']['did']['field'] = 'did';
  $handler->display->display_options['sorts']['did']['order'] = 'DESC';
  /* Filter criterion: Drupal Logger: Message */
  $handler->display->display_options['filters']['message']['id'] = 'message';
  $handler->display->display_options['filters']['message']['table'] = 'drupalog';
  $handler->display->display_options['filters']['message']['field'] = 'message';
  $handler->display->display_options['filters']['message']['operator'] = 'contains';
  $handler->display->display_options['filters']['message']['group'] = 1;
  $handler->display->display_options['filters']['message']['exposed'] = TRUE;
  $handler->display->display_options['filters']['message']['expose']['operator_id'] = 'message_op';
  $handler->display->display_options['filters']['message']['expose']['label'] = 'Message';
  $handler->display->display_options['filters']['message']['expose']['operator'] = 'message_op';
  $handler->display->display_options['filters']['message']['expose']['identifier'] = 'message';
  $handler->display->display_options['filters']['message']['expose']['autocomplete_items'] = '10';
  $handler->display->display_options['filters']['message']['expose']['autocomplete_min_chars'] = '0';
  $handler->display->display_options['filters']['message']['expose']['autocomplete_field'] = 'message';
  $handler->display->display_options['filters']['message']['expose']['autocomplete_raw_suggestion'] = 1;
  $handler->display->display_options['filters']['message']['expose']['autocomplete_raw_dropdown'] = 1;
  $handler->display->display_options['filters']['message']['expose']['autocomplete_dependent'] = 0;
  /* Filter criterion: Drupal Logger: Severity */
  $handler->display->display_options['filters']['severity']['id'] = 'severity';
  $handler->display->display_options['filters']['severity']['table'] = 'drupalog';
  $handler->display->display_options['filters']['severity']['field'] = 'severity';
  $handler->display->display_options['filters']['severity']['group'] = 1;
  $handler->display->display_options['filters']['severity']['exposed'] = TRUE;
  $handler->display->display_options['filters']['severity']['expose']['operator_id'] = 'severity_op';
  $handler->display->display_options['filters']['severity']['expose']['label'] = 'Severity';
  $handler->display->display_options['filters']['severity']['expose']['operator'] = 'severity_op';
  $handler->display->display_options['filters']['severity']['expose']['identifier'] = 'severity';
  $handler->display->display_options['filters']['severity']['expose']['multiple'] = TRUE;
  /* Filter criterion: Drupal Logger: Type */
  $handler->display->display_options['filters']['type_2']['id'] = 'type_2';
  $handler->display->display_options['filters']['type_2']['table'] = 'drupalog';
  $handler->display->display_options['filters']['type_2']['field'] = 'type';
  $handler->display->display_options['filters']['type_2']['group'] = 1;
  $handler->display->display_options['filters']['type_2']['exposed'] = TRUE;
  $handler->display->display_options['filters']['type_2']['expose']['operator_id'] = 'type_2_op';
  $handler->display->display_options['filters']['type_2']['expose']['label'] = 'Type';
  $handler->display->display_options['filters']['type_2']['expose']['operator'] = 'type_2_op';
  $handler->display->display_options['filters']['type_2']['expose']['identifier'] = 'type_2';
  $handler->display->display_options['filters']['type_2']['expose']['multiple'] = TRUE;
  /* Filter criterion: User: Name */
  $handler->display->display_options['filters']['uid']['id'] = 'uid';
  $handler->display->display_options['filters']['uid']['table'] = 'users';
  $handler->display->display_options['filters']['uid']['field'] = 'uid';
  $handler->display->display_options['filters']['uid']['relationship'] = 'uid';
  $handler->display->display_options['filters']['uid']['value'] = '';
  $handler->display->display_options['filters']['uid']['group'] = 1;
  $handler->display->display_options['filters']['uid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['uid']['expose']['operator_id'] = 'uid_op';
  $handler->display->display_options['filters']['uid']['expose']['label'] = 'User';
  $handler->display->display_options['filters']['uid']['expose']['operator'] = 'uid_op';
  $handler->display->display_options['filters']['uid']['expose']['identifier'] = 'uid';
  /* Filter criterion: Drupal Logger: Ip address */
  $handler->display->display_options['filters']['ip_address']['id'] = 'ip_address';
  $handler->display->display_options['filters']['ip_address']['table'] = 'drupalog';
  $handler->display->display_options['filters']['ip_address']['field'] = 'ip_address';
  $handler->display->display_options['filters']['ip_address']['group'] = 1;
  $handler->display->display_options['filters']['ip_address']['exposed'] = TRUE;
  $handler->display->display_options['filters']['ip_address']['expose']['operator_id'] = 'ip_address_op';
  $handler->display->display_options['filters']['ip_address']['expose']['label'] = 'Ip address';
  $handler->display->display_options['filters']['ip_address']['expose']['operator'] = 'ip_address_op';
  $handler->display->display_options['filters']['ip_address']['expose']['identifier'] = 'ip_address';
  $handler->display->display_options['filters']['ip_address']['expose']['autocomplete_items'] = '10';
  $handler->display->display_options['filters']['ip_address']['expose']['autocomplete_min_chars'] = '0';
  $handler->display->display_options['filters']['ip_address']['expose']['autocomplete_field'] = 'ip_address';
  $handler->display->display_options['filters']['ip_address']['expose']['autocomplete_raw_suggestion'] = 1;
  $handler->display->display_options['filters']['ip_address']['expose']['autocomplete_raw_dropdown'] = 1;
  $handler->display->display_options['filters']['ip_address']['expose']['autocomplete_dependent'] = 0;
  /* Filter criterion: Drupal Logger: $_GET */
  $handler->display->display_options['filters']['get_request']['id'] = 'get_request';
  $handler->display->display_options['filters']['get_request']['table'] = 'drupalog';
  $handler->display->display_options['filters']['get_request']['field'] = 'get_request';
  $handler->display->display_options['filters']['get_request']['operator'] = 'contains';
  $handler->display->display_options['filters']['get_request']['group'] = 1;
  $handler->display->display_options['filters']['get_request']['exposed'] = TRUE;
  $handler->display->display_options['filters']['get_request']['expose']['operator_id'] = 'get_request_op';
  $handler->display->display_options['filters']['get_request']['expose']['label'] = 'GET Request';
  $handler->display->display_options['filters']['get_request']['expose']['operator'] = 'get_request_op';
  $handler->display->display_options['filters']['get_request']['expose']['identifier'] = 'get_request';
  $handler->display->display_options['filters']['get_request']['expose']['autocomplete_items'] = '10';
  $handler->display->display_options['filters']['get_request']['expose']['autocomplete_min_chars'] = '0';
  $handler->display->display_options['filters']['get_request']['expose']['autocomplete_raw_suggestion'] = 1;
  $handler->display->display_options['filters']['get_request']['expose']['autocomplete_raw_dropdown'] = 1;
  $handler->display->display_options['filters']['get_request']['expose']['autocomplete_dependent'] = 0;
  /* Filter criterion: Drupal Logger: $_POST */
  $handler->display->display_options['filters']['post_request']['id'] = 'post_request';
  $handler->display->display_options['filters']['post_request']['table'] = 'drupalog';
  $handler->display->display_options['filters']['post_request']['field'] = 'post_request';
  $handler->display->display_options['filters']['post_request']['operator'] = 'contains';
  $handler->display->display_options['filters']['post_request']['group'] = 1;
  $handler->display->display_options['filters']['post_request']['exposed'] = TRUE;
  $handler->display->display_options['filters']['post_request']['expose']['operator_id'] = 'post_request_op';
  $handler->display->display_options['filters']['post_request']['expose']['label'] = 'POST Request';
  $handler->display->display_options['filters']['post_request']['expose']['operator'] = 'post_request_op';
  $handler->display->display_options['filters']['post_request']['expose']['identifier'] = 'post_request';
  $handler->display->display_options['filters']['post_request']['expose']['autocomplete_items'] = '10';
  $handler->display->display_options['filters']['post_request']['expose']['autocomplete_min_chars'] = '0';
  $handler->display->display_options['filters']['post_request']['expose']['autocomplete_raw_suggestion'] = 1;
  $handler->display->display_options['filters']['post_request']['expose']['autocomplete_raw_dropdown'] = 1;
  $handler->display->display_options['filters']['post_request']['expose']['autocomplete_dependent'] = 0;
  /* Filter criterion: Drupal Logger: $_COOKIE */
  $handler->display->display_options['filters']['cookie']['id'] = 'cookie';
  $handler->display->display_options['filters']['cookie']['table'] = 'drupalog';
  $handler->display->display_options['filters']['cookie']['field'] = 'cookie';
  $handler->display->display_options['filters']['cookie']['operator'] = 'contains';
  $handler->display->display_options['filters']['cookie']['group'] = 1;
  $handler->display->display_options['filters']['cookie']['exposed'] = TRUE;
  $handler->display->display_options['filters']['cookie']['expose']['operator_id'] = 'cookie_op';
  $handler->display->display_options['filters']['cookie']['expose']['label'] = 'COOKIE variables';
  $handler->display->display_options['filters']['cookie']['expose']['operator'] = 'cookie_op';
  $handler->display->display_options['filters']['cookie']['expose']['identifier'] = 'cookie';
  $handler->display->display_options['filters']['cookie']['expose']['autocomplete_items'] = '10';
  $handler->display->display_options['filters']['cookie']['expose']['autocomplete_min_chars'] = '0';
  $handler->display->display_options['filters']['cookie']['expose']['autocomplete_raw_suggestion'] = 1;
  $handler->display->display_options['filters']['cookie']['expose']['autocomplete_raw_dropdown'] = 1;
  $handler->display->display_options['filters']['cookie']['expose']['autocomplete_dependent'] = 0;
  /* Filter criterion: Drupal Logger: $_SESSION */
  $handler->display->display_options['filters']['session']['id'] = 'session';
  $handler->display->display_options['filters']['session']['table'] = 'drupalog';
  $handler->display->display_options['filters']['session']['field'] = 'session';
  $handler->display->display_options['filters']['session']['operator'] = 'contains';
  $handler->display->display_options['filters']['session']['group'] = 1;
  $handler->display->display_options['filters']['session']['exposed'] = TRUE;
  $handler->display->display_options['filters']['session']['expose']['operator_id'] = 'session_op';
  $handler->display->display_options['filters']['session']['expose']['label'] = 'SESSION variables';
  $handler->display->display_options['filters']['session']['expose']['operator'] = 'session_op';
  $handler->display->display_options['filters']['session']['expose']['identifier'] = 'session';
  $handler->display->display_options['filters']['session']['expose']['autocomplete_items'] = '10';
  $handler->display->display_options['filters']['session']['expose']['autocomplete_min_chars'] = '0';
  $handler->display->display_options['filters']['session']['expose']['autocomplete_raw_suggestion'] = 1;
  $handler->display->display_options['filters']['session']['expose']['autocomplete_raw_dropdown'] = 1;
  $handler->display->display_options['filters']['session']['expose']['autocomplete_dependent'] = 0;
  /* Filter criterion: From date */
  $handler->display->display_options['filters']['date_time']['id'] = 'date_time';
  $handler->display->display_options['filters']['date_time']['table'] = 'drupalog';
  $handler->display->display_options['filters']['date_time']['field'] = 'date_time';
  $handler->display->display_options['filters']['date_time']['ui_name'] = 'From date';
  $handler->display->display_options['filters']['date_time']['operator'] = '>=';
  $handler->display->display_options['filters']['date_time']['exposed'] = TRUE;
  $handler->display->display_options['filters']['date_time']['expose']['operator_id'] = 'date_time_op';
  $handler->display->display_options['filters']['date_time']['expose']['label'] = 'From date';
  $handler->display->display_options['filters']['date_time']['expose']['operator'] = 'date_time_op';
  $handler->display->display_options['filters']['date_time']['expose']['identifier'] = 'date_time';
  /* Filter criterion: Drupal Logger: Date */
  $handler->display->display_options['filters']['date_time_1']['id'] = 'date_time_1';
  $handler->display->display_options['filters']['date_time_1']['table'] = 'drupalog';
  $handler->display->display_options['filters']['date_time_1']['field'] = 'date_time';
  $handler->display->display_options['filters']['date_time_1']['operator'] = '<=';
  $handler->display->display_options['filters']['date_time_1']['exposed'] = TRUE;
  $handler->display->display_options['filters']['date_time_1']['expose']['operator_id'] = 'date_time_1_op';
  $handler->display->display_options['filters']['date_time_1']['expose']['label'] = 'To date';
  $handler->display->display_options['filters']['date_time_1']['expose']['operator'] = 'date_time_1_op';
  $handler->display->display_options['filters']['date_time_1']['expose']['identifier'] = 'date_time_1';
  /* Display: Archive */
  $handler = $view->new_display('page', 'Archive', 'archive_page');
  $handler->display->display_options['path'] = 'admin/reports/drupalog/archive';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Archive';
  $handler->display->display_options['menu']['description'] = 'View drupal logger logs archive.';
  $handler->display->display_options['menu']['weight'] = '1';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $translatables['drupal_logger_archive'] = array(
    t('Master'),
    t('Archive'),
    t('more'),
    t('Filter'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Select any filter and click on Apply to see results'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« first'),
    t('‹ previous'),
    t('next ›'),
    t('last »'),
    t('Users relationship'),
    t('.'),
    t('Date'),
    t('Message'),
    t('Type'),
    t('Severity'),
    t('User'),
    t('Anonym'),
    t('URL'),
    t('Referer'),
    t('Ip address'),
    t('GET Request'),
    t('POST Request'),
    t('COOKIE variables'),
    t('REQUEST variables'),
    t('SESSION variables'),
    t('From date'),
    t('To date'),
  );

  return array(
    $view->name => $view,
  );
}
