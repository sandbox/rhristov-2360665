<?php

/**
 * Function for showing latest log messages realtime.
 *
 * @param array $form The form.
 * @param array $form_state Te form state.
 *
 * @return array
 */
function drupalog_realtime_page(array $form = array(), array $form_state = array()) {
  $last_logs_data = _drupalog_realtime_page_get_last_logs_data();
  $severities = array();
  $minimal_log_severity = drupalog_variable_get('logs_minimal_severity', \Drupalog::SEVERITY_DEBUG);
  foreach (\Drupalog::$severities as $id => $name) {
    if ($id >= $minimal_log_severity) {
      $severities[$id] = $name;
    }
  }

  return array(
    '#attached' => array(
      'js' => array(
        drupal_get_path('module', 'drupalog') . '/js/drupalog.common.js',
        drupal_get_path('module', 'drupalog') . '/js/drupalog.realtime.page.js',
        array(
          'data' => array('drupalog' => array(
              'logs' => $last_logs_data['logs'],
              'severities' => \Drupalog::$severities,
              'last_log_id' => $last_logs_data['last_log_id'],
              'logs_count' => drupalog_variable_get('logs_preview_count', 100),
            )),
          'type' => 'setting',
        ),
      ),
      'css' => array(
        drupal_get_path('module', 'drupalog') . '/css/drupalog.realtime.page.css',
        drupal_get_path('module', 'drupalog') . '/css/drupalog.common.css',
      ),
    ),
    'settings' => array(
      '#type' => 'fieldset',
      '#title' => t('Filter Log Messages'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      'clear_button' => array(
        '#type' => 'button',
        '#value' => t('Clear current logs'),
        '#id' => 'clear-logs',
      ),
      'current_logs_shown_severities' => array(
        '#type' => 'select',
        '#multiple' => TRUE,
        '#size' => 7,
        '#options' => array_replace(
            array('no' => t('Logs severities')), $severities
        ),
        '#id' => 'logs-severities',
        '#default_value' => array_merge(array('no'), array_keys($severities)),
      ),
      'current_logs_types' => array(
        '#type' => 'select',
        '#multiple' => TRUE,
        '#size' => 7,
        '#options' => array_replace(
            array('no' => t('Logs types')), $last_logs_data['log_types']
        ),
        '#id' => 'logs-types',
        '#default_value' => array_merge(array('no'), array_keys($last_logs_data['log_types'])),
      ),
    ),
    'logs_table' => _drupalog_realtime_page_build_table(),
  );
}

/**
 * Helper function for getting last logs data.
 *
 * @return array
 */
function _drupalog_realtime_page_get_last_logs_data() {
  $logs = array();
  $last_log_id = NULL;
  $log_types = array();

  $fetched_logs = db_select(\DrupalogEntity::getTableName())
      ->fields('drupalog')
      ->range(0, drupalog_variable_get('logs_preview_count', 100))
      ->condition('drupalog.severity', drupalog_variable_get('logs_minimal_severity', \Drupalog::SEVERITY_DEBUG), '>=')
      ->orderBy('drupalog.did', 'DESC')
      ->execute()
      ->fetchAll(PDO::FETCH_CLASS, 'DrupalogEntity');
  foreach ($fetched_logs as $key => $log) {
    $logs[] = $log->toArray(TRUE);

    $log_types[$log->getType()] = $log->getType();

    if (0 === $key) {
      $last_log_id = $log->getDid();
    }
  }

  return array(
    'logs' => $logs,
    'last_log_id' => $last_log_id,
    'log_types' => $log_types,
  );
}

/**
 * Helper function for rendering logs table.
 *
 * @return array
 */
function _drupalog_realtime_page_build_table() {
  $logs_table = theme('table', array(
    'header' => array(
      t('Date'),
      t('Message'),
      t('Type'),
      t('Severity'),
      t('User'),
      t('URL'),
      t('Referer'),
      t('Ip address'),
    ),
    'rows' => array(),
    'attributes' => array(
      'id' => 'logs_table',
      'class' => 'table',
    ),
    'caption' => NULL,
    'colgroups' => array(),
    'sticky' => FALSE,
    'empty' => '',
  ));

  return array(
    '#type' => 'markup',
    '#markup' => render($logs_table),
  );
}
