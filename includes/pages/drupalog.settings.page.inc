<?php

/**
 * Function for configuring drupal logger.
 *
 * @param array $form
 * @param array $form_state
 *
 * @return array
 */
function drupalog_settings_page(array $form = array(), array $form_state = array()) {
  return system_settings_form(array(
    '#submit' => array('drupalog_settings_page_submit'),
    'drupalog' => array(
      '#type' => 'fieldset',
      '#tree' => TRUE,
      '#title' => t('Edit settings'),
      'logs_preview_count' => array(
        '#type' => 'select',
        '#title' => t('Realtime logger logs preview count.'),
        '#options' => array(
          10 => 10,
          25 => 25,
          50 => 50,
          100 => 100,
          150 => 150,
          200 => 200,
          250 => 250,
          500 => 500,
        ),
        '#default_value' => drupalog_variable_get('logs_preview_count', 100),
        '#required' => TRUE,
      ),
      'logs_minimal_severity' => array(
        '#type' => 'select',
        '#title' => t('Logs minimal severity.'),
        '#description' => t('Logs minimal severity by which log will be recorded and listed/sended per mail.'),
        '#options' => \Drupalog::$severities,
        '#default_value' => drupalog_variable_get('logs_minimal_severity', \Drupalog::SEVERITY_DEBUG),
        '#required' => TRUE,
      ),
      'mails_minimal_severity' => array(
        '#type' => 'select',
        '#title' => t('Mail sending minimal severity.'),
        '#description' => t('Minimal severity by which mail will be send on log record.'),
        '#options' => \Drupalog::$severities,
        '#default_value' => drupalog_variable_get('mails_minimal_severity', \Drupalog::SEVERITY_CRITICAL),
        '#required' => TRUE,
      ),
      'mails_addresses' => array(
        '#type' => 'textfield',
        '#title' => t('Mail sending email addresses.'),
        '#description' => t('Comma separated list of email addresses, which will receive mail notifications on log record. If not populated, no mails will be send.'),
        '#default_value' => drupalog_variable_get('mails_addresses', ''),
        '#required' => FALSE,
      ),
      'mails_last_log_id' => array(
        '#type' => 'hidden',
        '#default_value' => drupalog_variable_get('mails_last_log_id', 0),
        '#required' => FALSE,
      ),
    ),
    'delete_fieldset' => array(
      '#type' => 'fieldset',
      '#title' => t('Delete all logs'),
      'delete_logs' => array(
        '#type' => 'submit',
        '#value' => t('Delete'),
      ),
    ),
  ));
}

function drupalog_settings_page_submit(array $form = array(), array $form_state = array()) {
  if (_form_button_was_clicked($form['delete_fieldset']['delete_logs'], $form_state)) {
    db_truncate(\DrupalogEntity::getTableName())
        ->execute();
    drupalog_variable_set('mails_last_log_id', 0);
  }
}
