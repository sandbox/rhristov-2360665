<?php

/**
 * Page callback for showing detailed information about single log data.
 *
 * @param int $log_id
 *
 * @return string
 */
function drupalog_details_page($log_id) {
  $log = db_select(DrupalogEntity::getTableName())
      ->fields(DrupalogEntity::getTableName())
      ->condition('did', (int) $log_id)
      ->execute()
      ->fetchObject('DrupalogEntity');
  if (FALSE === $log) {
    return drupal_not_found();
  }

  $log = $log->toArray(TRUE);

  drupal_add_js(array('drupalog' => array('log' => $log)), 'setting');
  drupal_add_js(drupal_get_path('module', 'drupalog') . '/js/drupalog.common.js');
  drupal_add_js(drupal_get_path('module', 'drupalog') . '/js/drupalog.details.page.js');

  drupal_add_css(drupal_get_path('module', 'drupalog') . '/css/drupalog.common.css');

  return theme('drupalog_details_page', array(
    'log' => $log,
  ));
}
