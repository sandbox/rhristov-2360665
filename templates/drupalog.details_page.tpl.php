<?php

/**
 * @file
 * Template for drupalog detailed page preview.
 */

?>
<table class="table">
  <thead>
    <tr>
      <th width="14%"><?php print t('Date'); ?></th>
      <th width="14%"><?php print t('Type'); ?></th>
      <th width="14%"><?php print t('Severity'); ?></th>
      <th width="14%"><?php print t('User'); ?></th>
      <th width="14%"><?php print t('URL'); ?></th>
      <th width="14%"><?php print t('Referer'); ?></th>
      <th width="14%"><?php print t('Ip address'); ?></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><?php print $log['date_time']; ?></td>
      <td><?php print $log['type']; ?></td>
      <td><?php print $log['severity']; ?></td>
      <td><?php print $log['uid'] ? $log['uid'] : t('Anonym (not verified)'); ?></td>
      <td><?php print $log['url']; ?></td>
      <td><?php print $log['http_referer']; ?></td>
      <td><?php print $log['ip_address']; ?></td>
    </tr>
    <tr>
      <th colspan="7"><center><?php print t('Message'); ?></center></th>
    </tr>
    <tr>
      <td colspan="7"><?php print $log['message']; ?></td>
    </tr>
    <tr>
      <th colspan="2"><?php print t('GET parameters'); ?></th>
      <th colspan="2"><?php print t('POST parameters'); ?></th>
      <th colspan="2"><?php print t('SESSION parameters'); ?></th>
      <th><?php print t('COOKIE parameters'); ?></th>
    </tr>
    <tr>
      <td colspan="2"><pre id="get_request"></pre></td>
      <td colspan="2"><pre id="post_request"></pre></td>
      <td colspan="2"><pre id="session"></pre></td>
      <td><pre id="cookie"></pre></td>
    </tr>
    <tr>
      <th colspan="7"><center><?php print t('Stacktrace'); ?></center></th>
    </tr>
    <tr>
      <td colspan="7"><pre id="stacktrace"></pre></td>
    </tr>
  </tbody>
</table>
