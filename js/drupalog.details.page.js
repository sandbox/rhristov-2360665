/**
 * File for storing details page javascript functions.
 */

(function($) {
  Drupal.behaviors.drupalogDetailsPage = {
    attach: function (context, settings) {
      $('body').once('drupalogDetailsPage', function() {
        var log = settings.drupalog.log;
        $('#get_request').html(window.prettyPrintLibrary.json.prettyPrint(log.get_request));
        $('#post_request').html(window.prettyPrintLibrary.json.prettyPrint(log.post_request));
        $('#session').html(window.prettyPrintLibrary.json.prettyPrint(log.session));
        $('#cookie').html(window.prettyPrintLibrary.json.prettyPrint(log.cookie));
        $('#stacktrace').html(window.prettyPrintLibrary.json.prettyPrint(log.stacktrace));
      });
    }
  };
})(jQuery);
