(function($) {
  Drupal.behaviors.drupalogRealtimePage = {
    attach: function (context, settings) {
      $('body').once('drupalogRealtimePage', function() {
        function DrupalogPage (drupalogSettings) {
          this.addLog = function (log, isNew) {
            try {
              var td = '';
              td += '<td>' + log.date_time + '</td>';
              td += '<td>' + this.helpers.stripTags(log.message).substring(0, 120) + (this.helpers.stripTags(log.message).length > 120 ? '...' : '') + '</td>';
              td += '<td>' + log.type + '</td>';
              td += '<td>' + log.severity + '</td>';
              td += '<td>' + log.uid + '</td>';
              td += '<td>' + log.url.substring(0, 40) + '</td>';
              td += '<td>' + (log.http_referer ? log.http_referer.substring(0, 40) : '') + '</td>';
              td += '<td>' + log.ip_address + '</td>';

              var tr = $('<tr />', {
                'class': 'logs-table-row drupalog-' + log.severity.toLowerCase(),
                'id': 'logs-table-row-' + this.lastLogIndex,
                'data-log-id': this.lastLogIndex,
                'title': Drupal.t('Expand row')
              }).html(td);

              if (isNew) {      
                var tableRow = this.tableContainer.find('tr:first');
                if (tableRow.next()) {
                  while(tableRow.next().hasClass('pinned') && tableRow.next() && tableRow.next().length) {
                    tableRow = tableRow.next();
                  }
                }
                tr.insertAfter(tableRow);
              } else {
                tr.appendTo(this.tableContainer);
              }

              this.helpers.attachBindExpandCollapseTableRowEvent($('#logs-table-row-' + this.lastLogIndex));

              this.logs[this.lastLogIndex] = log;

              this.lastLogIndex++;
            } catch (e) {
              console.log(e);
            }
          },
          this.filterSeverities = function (severities) {
            drupalogPage.tableContainer.find('tr').each(function (index) {
              if (
                index === 0 ||
                severities.length === 0 ||
                $.inArray($(this).find('td:nth-child(4)').text(), severities) !== -1 ||
                $(this).hasClass('pinned') ||
                $(this).parents('table:first').attr('id') === 'logs-table-additional-info' ||
                $(this).prev().hasClass('expanded')
              ) {
                $(this).removeClass('severity-hidden');
              } else {
                $(this).addClass('severity-hidden');
                $(this).removeClass('expanded');
                if (!$(this).hasClass('logs-table-row')) {
                  $(this).remove();
                }
              }
            });
          },
          this.filterTypes = function (types) {
            drupalogPage.tableContainer.find('tr').each(function (index) {
              if (
                index === 0 ||
                types.length === 0 ||
                $.inArray($(this).find('td:nth-child(3)').text(), types) !== -1 ||
                $(this).hasClass('pinned') ||
                $(this).parents('table:first').attr('id') === 'logs-table-additional-info' ||
                $(this).prev().hasClass('expanded')
              ) {
                $(this).removeClass('type-hidden');
              } else {
                $(this).addClass('type-hidden');
                $(this).removeClass('expanded');
                if (!$(this).hasClass('logs-table-row')) {
                  $(this).remove();
                }
              }
            });
          },
          this.getNewLogs = function () {
            var classInstance = this;
            var ajaxCallInProgress = true;
            var ajaxCallLongerResponseMessageIsShown = false;

            window.setTimeout(function () {
              if (ajaxCallInProgress) {
                classInstance.helpers.showMessage(Drupal.t('Fetching new logs takes longer than expected - more than 25 seconds. Please reload the page or wait for response.', {}), 'warning');
                ajaxCallLongerResponseMessageIsShown = true;
              }
            }, 25000);
            $.getJSON('/admin/reports/drupalog/service/load-more/' + classInstance.lastLogId, function(data) {
              if (data.logs.length > 0) {
                classInstance.lastLogId = data.last_log_id;
                for (var i in data.logs) {
                  classInstance.addLog(data.logs[i], true);
                }
                var rowsCount = 0;
                classInstance.tableContainer.find('tr').each(function() {
                  rowsCount++;
                  if (rowsCount > classInstance.logsCount) {
                    $(this).remove();
                  }
                });

                $(data.log_types).each(function (index, value) {
                  if ($('#logs-types option[value="' + value + '"]').length == 0) {
                    $('#logs-types').append('<option selected="selected" value="' + value + '">' + value + '</option>');
                  }
                });

                if (!$('#logs-types option[value="no"]').is(':selected')) {
                  $('#logs-types').trigger('change');
                }
                $('#logs-severities').trigger('change');
              }
             window.setTimeout(function() {
                classInstance.getNewLogs();
              }, 3000);
              ajaxCallInProgress = false;
              if (ajaxCallLongerResponseMessageIsShown) {
                classInstance.helpers.hideMessage();
              }
            }).fail(function(response) {
              ajaxCallInProgress = false;
              if (ajaxCallLongerResponseMessageIsShown) {
                classInstance.helpers.hideMessage();
              }
              classInstance.helpers.showMessage(Drupal.t('There was an error by searching for new logs - server responded with status !status. New attempt will be made after 30 seconds.', {'!status': response.status}), 'error');
              window.setTimeout(function() {
                classInstance.helpers.hideMessage();
                classInstance.getNewLogs();
              }, 30000);

            });
          },
          this.init = function (drupalogSettings) {
            var classInstance = this;

            classInstance.logs = [];
            classInstance.severities = drupalogSettings.severities;
            classInstance.lastLogId = !drupalogSettings.last_log_id || isNaN(drupalogSettings.last_log_id) ? 0 : drupalogSettings.last_log_id;
            classInstance.lastLogIndex = 0;
            classInstance.logsCount = drupalogSettings.logs_count;
            classInstance.tableContainer = $('#logs_table').find('tbody') ? $('#logs_table').find('tbody') : $('#logs_table');

            window.setTimeout (function() {
              classInstance.getNewLogs();
            }, 3000);

            for (var i in settings.drupalog.logs) {
              classInstance.addLog(settings.drupalog.logs[i]);
            }
          },
          this.helpers = {
            classInstance: this,
            attachBindExpandCollapseTableRowEvent: function (tableRow) {
              var classInstance = this.classInstance;
              tableRow.bind('click', function (e) {
                var row = $(this);
                var log = classInstance.logs[row.attr('data-log-id')];

                if (!row.hasClass('expanded')) {
                  row.attr('title', Drupal.t('Collapse row'));
                  var expandedInformationTableRow = $('<tr />');
                  expandedInformationTableRow.css({display: 'none'});

                  var html = '';
                  html += '<table class="table" id="logs-table-additional-info">';
                  html += '  <thead>';
                  html += '    <tr>';
                  html += '      <th colspan="4"><input type="button" value="' + Drupal.t('Pin log!') + '" class="form-submit pin-log" title="' + Drupal.t('Pin this log to the top of the table!') + '" /> ' + log.message + '</th>';
                  html += '    </tr>';
                  html += '  </thead>';
                  html += '  <tbody>';
                  html += '    <tr><th colspan="2">' + Drupal.t('URL') + '</th><th colspan="2">' + Drupal.t('HTTP REFERER') + '</th></tr>';
                  html += '    <tr><td colspan="2"><a href="' + log.url + '" target="_blank">' + log.url + '</a></td><td colspan="2">' + (log.http_referer && log.http_referer.length ? '<a href="' + log.http_referer + '" target="_blank">' + log.http_referer + '</a>' : '') + '</td></tr>';
                  html += '    <tr><th>' + Drupal.t('GET Parameters') + '</th><th>' + Drupal.t('POST Parameters') + '</th><th>' + Drupal.t('SESSION Parameters') + '</th><th>' + Drupal.t('COOKIE Parameters') + '</th></tr>';
                  html += '    <tr><td><pre>' + window.prettyPrintLibrary.json.prettyPrint(log.get_request) + '</pre></td><td><pre>' + window.prettyPrintLibrary.json.prettyPrint(log.post_request) + '</pre></td><td><pre>' + window.prettyPrintLibrary.json.prettyPrint(log.session) + '</pre></td><td><pre>' + window.prettyPrintLibrary.json.prettyPrint(log.cookie) + '</pre></td></tr>';
                  html += '    <tr><th colspan="4" align="center"><center>' + Drupal.t('Stacktrace') + '</center></th></tr>';
                  html += '    <tr><td colspan="4"><pre>' + window.prettyPrintLibrary.json.prettyPrint(log.stacktrace) + '</pre></td></tr>';
                  html += '  </tbody>';
                  html += '</table>';

                  expandedInformationTableRow.append('<td colspan="8">' + html + '</td>');
                  expandedInformationTableRow.insertAfter($('#logs-table-row-' + row.attr('data-log-id')));
                  expandedInformationTableRow.fadeIn('slow');

                  row.addClass('expanded');

                  classInstance.helpers.attachPinLogEvent(expandedInformationTableRow);
                } else {
                  row.next().fadeOut(function() {
                    $(this).remove();
                  });
                  row.removeClass('expanded');
                  row.removeClass('pinned');
                  row.attr('title', Drupal.t('Expand row'));
                }
              });
            },
            attachPinLogEvent: function (expandedTableRow) {
              var classInstance = this.classInstance;

              expandedTableRow.find('input.pin-log').bind('click', function (e) {
                e.preventDefault();

                var mainTableRow = expandedTableRow.prev();

                if (expandedTableRow.hasClass('pinned')) {
                  $(this).val('Pin log!');

                  mainTableRow.removeClass('pinned');
                  expandedTableRow.removeClass('pinned');
                } else {
                  $(this).val('Unpin log!');

                  mainTableRow.insertAfter(classInstance.tableContainer.find('tr:first'));
                  expandedTableRow.insertAfter(mainTableRow);

                  mainTableRow.addClass('pinned');
                  expandedTableRow.addClass('pinned');

                   $('html, body').animate({ scrollTop: 0 }, 'slow');
                }

                return false;
              });
            },
            hideMessage: function () {
              $('#drupalog-message').remove();
            },
            stripTags: function (html) {
              var div = document.createElement("div");
              div.innerHTML = html;
              var text = div.textContent || div.innerText || "";
              return text;
            },
            showMessage: function (message, status) {
              this.classInstance.helpers.hideMessage();
              $('<div id="drupalog-message" class="messages ' + status + ' clearfix">' + message + '</div>').insertBefore('div.region-content');
            },
            strPad: function (length, value, padding) {
              return (value.toString().length < length) ? this.strPad(length, padding + value, padding) : value;
            },
          },
          this.reset = function () {
            this.logs = [];
            this.lastLogId = 0;
            this.lastLogIndex = 0;
          };

          this.init(drupalogSettings);
        }

        var drupalogPage = new DrupalogPage(settings.drupalog);

        $('#clear-logs').bind('click', function (e) {
          e.preventDefault();
          $('#logs_table').find('tr').each(function (index) {
             if (index !== 0) {
                 $(this).remove();
             }
          });
          return false;
        });

        $('#logs-severities').bind('change', function () {
          var severities = [];

          if ($('#logs-severities option[value="no"]').is(':selected')) {
            $('#logs-severities option').attr('selected', 'selected');
          }

          $($(this).val()).each(function (index, value) {
            if (typeof drupalogPage.severities[value] != 'undefined') {
              severities.push(drupalogPage.severities[value]);
            }
          });

          drupalogPage.filterSeverities(severities);
        });

        $('#logs-types').bind('change', function () {
          var types = [];

          if ($('#logs-types option[value="no"]').is(':selected')) {
            $('#logs-types option').attr('selected', 'selected');
          }

          $($(this).val()).each(function (index, value) {
            if (value !== 'no') {
              types.push(value);
            }
          });

          drupalogPage.filterTypes(types);
        });
      });
    }
  };
})(jQuery);
